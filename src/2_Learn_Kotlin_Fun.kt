import kotlin.random.Random

fun main() {
    println(multiply3(123, 4235)) //Передаю параметры в функцию с пара-ми по умолчанию
    println(functionReturnAny("sgfkjsdg")) //Эта и функция ниже принемают разные типы, но работают корректно
    println(functionReturnAny(25367.1))
}

//Block body with value
fun multiply(a: Int, b: Int) : Int {
    val result = a * b
    return result
}

//Block body not have value and one value disable
fun multiply2(a: Int, b: Int = 10) : Int {
    return a * b
}

//Expresion body with return type and two value disable
fun multiply3(a: Int = 5, b: Int = 10) : Int = a * b

//Expresion body not have return type
fun multiply4(a: Int, b: Int)  = a * b

//Block body bool
fun hasChildreAccess(height: Int, weight: Int, age: Int) : Boolean{
    return height > 150 && weight > 30 && age > 10
}
//Block body with Any parametrs and return type Any
fun functionReturnAny(a : Any) : Any {
    return if(Random.nextBoolean()) {
        println(a) //для показа входного параметра
        1// if generate "true" return Int
    } else {
        println(a) //для показа входного параметра
        "else" // if generate "false" return String
    }
}

//Unit function analog void
fun funReturnUnit() : Unit {
    return
}


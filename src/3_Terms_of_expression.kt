fun main() {

}
fun maxInt(a: Int, b: Int): Int {
    val maxValue: Int
    if (a < b) {
        maxValue = b
    } else {
        maxValue = a
    }
    return maxValue
}

fun maxInt2(a: Int, b: Int): Int {
    val maxValue: Int
    if (a < b) maxValue = b else  maxValue = a
    return  maxValue
}

fun maxInt3(a: Int, b: Int): Int {
    val maxValue: Int = if (a < b) b else a
    return maxValue
    /* return if (a < b) b else a */
}
//Expression body
fun maxInt4(a: Int, b:Int): Int = if (a < b) b else a

